<?php

namespace Drupal\salesforce_oauth_client_credentials\Plugin\SalesforceAuthProvider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\salesforce\SalesforceAuthProviderPluginBase;
use OAuth\Common\Http\Uri\Uri;
use OAuth\Common\Token\TokenInterface;

/**
 * Salesforce OAuth client credentials flow auth provider plugin.
 *
 * @Plugin(
 *   id = "oauth",
 *   label = @Translation("Salesforce OAuth Client credentials"),
 *   credentials_class = "\Drupal\salesforce_oauth_client_credentials\Consumer\SalesforceOAuthClientCredentials"
 * )
 */
class SalesforceOAuthPlugin extends SalesforceAuthProviderPluginBase {

  /**
   * Credentials.
   *
   * @var \Drupal\salesforce_oauth_client_credentials\Consumer\SalesforceOAuthClientCredentials
   */
  protected $credentials;

  /**
   * {@inheritdoc}
   */
  public static function defaultConfiguration() {
    $defaults = parent::defaultConfiguration();
    return array_merge($defaults, [
      'consumer_secret' => '',
      'domain' => '',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['consumer_key'] = [
      '#title' => $this->t('Salesforce consumer key'),
      '#type' => 'textfield',
      '#description' => $this->t('Consumer key of the Salesforce remote application you want to grant access to'),
      '#required' => TRUE,
      '#default_value' => $this->getCredentials()->getConsumerKey(),
    ];

    $form['consumer_secret'] = [
      '#title' => $this->t('Salesforce consumer secret'),
      '#type' => 'textfield',
      '#description' => $this->t('Consumer secret of the Salesforce remote application.'),
      '#required' => TRUE,
      '#default_value' => $this->getCredentials()->getConsumerSecret(),
    ];

    $form['login_url'] = [
      '#title' => $this->t('Login URL'),
      '#type' => 'textfield',
      '#default_value' => $this->getCredentials()->getLoginUrl(),
      '#description' => $this->t('Enter a login URL, either https://login.salesforce.com or https://test.salesforce.com.'),
      '#required' => TRUE,
    ];

    $form['domain'] = [
      '#title' => $this->t('Domain URL'),
      '#type' => 'textfield',
      '#default_value' => $this->getCredentials()->getDomainUrl(),
      '#description' => $this->t('<b>NOTE:</b> To find your My Domain URL, from Setup, in the Quick Find box, enter My Domain, and then select My Domain.'),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $this->setConfiguration($form_state->getValue('provider_settings'));
    // Store the new credentials from the form input.
    try {
      $this->requestAccessToken(NULL);
    }
    catch (\Exception $e) {
      $form_state->setError($form, $e->getMessage());
    }
  }

  /**
   * Overrides AbstractService::requestAccessToken for client credentials flow.
   *
   * @param string $code
   *   The access code.
   * @param null $state
   *   The authorization state.
   *
   * @return \OAuth\Common\Token\TokenInterface
   *   Access Token.
   *
   * @throws \OAuth\Common\Http\Exception\TokenResponseException
   */
  public function requestAccessToken($code, $state = NULL) {
    $data = [
      'grant_type' => 'client_credentials',
      'client_id'     => $this->getCredentials()->getConsumerId(),
      'client_secret' => $this->getCredentials()->getConsumerSecret(),
    ];

    $response = $this->httpClient->retrieveResponse(new Uri($this->getCredentials()->getDomainUrl() . static::AUTH_TOKEN_PATH), $data, ['Content-Type' => 'application/x-www-form-urlencoded']);
    $token = $this->parseAccessTokenResponse($response);
    $this->storage->storeAccessToken($this->service(), $token);
    $this->refreshIdentity($token);
    return $token;
  }

  /**
   * {@inheritDoc}
   */
  public function refreshAccessToken(TokenInterface $token) {
    $token = $this->requestAccessToken(NULL);
    $this->refreshIdentity($token);
    return $token;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerSecret() {
    return $this->getCredentials()->getConsumerSecret();
  }

}
