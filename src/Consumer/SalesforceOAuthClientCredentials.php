<?php

namespace Drupal\salesforce_oauth_client_credentials\Consumer;

use Drupal\Core\Url;
use Drupal\salesforce\Consumer\SalesforceCredentials;

/**
 * Salesforce oAuth client credentials flow.
 */
class SalesforceOAuthClientCredentials extends SalesforceCredentials {

  /**
   * The Salesforce account domain.
   *
   * @var string
   */
  protected $domain;

    /**
     * The Salesforce login URL.
     *
     * @var string
     */
    protected $loginUrl;

    /**
   * {@inheritdoc}
   */
  public function __construct($consumerKey, $consumerSecret, $loginUrl, $domain) {
    parent::__construct($consumerKey, $consumerSecret, $loginUrl);
    $this->loginUrl = $loginUrl;
    $this->domain = $domain;
  }

  /**
   * Constructor helper.
   *
   * @param array $configuration
   *   Plugin configuration.
   *
   * @return \Drupal\salesforce_oauth_client_credentials\Consumer\SalesforceOAuthClientCredentials
   *   Credentials, valid or not.
   */
  public static function create(array $configuration) {
    return new static($configuration['consumer_key'], $configuration['consumer_secret'], $configuration['login_url'], $configuration['domain']);
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    return !empty($this->loginUrl) && !empty($this->consumerSecret) && !empty($this->consumerId) && !empty($this->domain);
  }

  /**
   * Returns the Salesforce domain URL.
   *
   * @return string
   *   The SF domain URL.
   */
  public function getDomainUrl() {
    return $this->domain;
  }

}
